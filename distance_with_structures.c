//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h> 

struct pt //structure 
{ 	
	float x,y;
}p1,p2;

float valueX() //function to input x coordinate
{
	float a;
	printf("Enter x coordinate: ");
	scanf("%f",&a);
	return a;
}
float valueY() //function to input y coordinate
{
	float b;
	printf("Enter y coordinate: ");
	scanf("%f",&b);
	return b;
}
float distance(float a, float b, float c, float d) //func to calculate distance
{
	float dist;
	dist = (sqrt ( ( (a - c)*(a - c) ) + ( (b - d)*(b - d) )));
	return dist;
}
float print(float dist) //function to print final answer
{
	printf("The distance between the points is : %f\n",dist);
}
int main() //main function
{
	float result;
	printf("Point 1 -\n");
    p1.x = valueX();
    p1.y = valueY();
    printf("Point 2 -\n");
    p2.x = valueX();
    p2.y = valueY();
    result = distance(p1.x, p1.y, p2.x, p2.y);
    print(result); 
	return 0;
}