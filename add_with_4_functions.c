#include<stdio.h>
int input1() //first input
{
	int x;
    printf("Input first number: ");
    scanf("%d",&x);
    return x;
}
int input2() //second input
{
	int y;
    printf("Input  second number: ");
    scanf("%d",&y);
    return y;
}
int add(int x, int y) //function to add numbers
{
	int sum;
	sum = x + y;
	return sum;
}
int output(int sum)//function to print
{
	printf("The sum is:%d ", sum);
}
int main()
{
	int a,b, result;
	a = input1();
	b = input2();
	result = add(a,b);
	output(result);
	return 0;
}