//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int main()
{
	int n, sum=0;
	printf("Enter the number of elements you want to add:\n");
	scanf("%d",&n);
	for(int i=0;i<n;i++)
    {
    	int x;
		printf("Enter the number %d: ",i+1 );
		scanf("%d",&x); 
		sum = sum + x;
	}
	printf("Sum of these numbers is: %d",sum);
	return 0;
}

