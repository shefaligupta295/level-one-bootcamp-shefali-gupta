//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct fraction
{
	int numerator, denominator;
}fract;

fract input() //funtion to take input 
{
	fract f;
	printf("Enter numerator: ");
	scanf("%d",&f.numerator);
	printf("Enter denominator: ");
	scanf("%d",&f.denominator);
	return f;
}
int gcd(int x, int y) //function to reduce the fraction
{
	int g=1;
    for(int i=2; i <= x; i++)
	{
		if (x%i == 0 && y%i ==0)
		g = i;
	}
	return g;
}
fract calcfraction(fract x, fract y) //function to calculate fraction and reduce it
{
    fract sum;
    sum.numerator = ((x.numerator * y.denominator) + (y.numerator * x.denominator));
    sum.denominator = (x.denominator * y.denominator);
    int g = gcd(sum.numerator, sum.denominator);
    sum.numerator = (sum.numerator/g);
    sum.denominator = (sum.denominator/g);
    return sum;
}
void print(fract x , fract y , fract res) //function to print the answer
{
    printf("The sum of %d / %d and %d / %d is : %d / %d\n", x.numerator, x.denominator, y.numerator, y.denominator, res.numerator, res.denominator);
}
int main() 
{
    fract x, y, res;
	printf("Enter 1st fraction:\n");
	x = input();
	printf("Enter 2nd fraction:\n");
	y = input();
	res = calcfraction(x , y);
	print(x, y, res);
	return 0;
}
