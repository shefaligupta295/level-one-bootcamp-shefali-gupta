//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float valueX() //to input x coordinate
{
	float a;
	printf("Enter x coordinate: ");
	scanf("%f",&a);
	return a;
}
float valueY() //to input y coordinate
{
	float b;
	printf("Enter y coordinate: ");
	scanf("%f",&b);
	return b;
}
float distance(float a, float b, float c, float d) //func to calculate distance
{
	float dist;
	dist = (sqrt ( ( (a-c)*(a-c) ) + ( (b-d)*(b-d) )));
	return dist;
}
float print(float dist) //to print final answer
{
	printf("The distance between the points is : %f\n",dist);
}
int main() //main function
{
	float x1, y1, x2, y2, result;
    x1 = valueX();
    y1 = valueY();
    x2 = valueX();
    y2 = valueY();
    result = distance(x1,y1,x2,y2);
    print(result); 
	return 0;
}
