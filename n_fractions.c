//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct fraction
{
	int numerator, denominator;
}fract;

typedef struct nsfract
{
	fract nf[1000];
}nfrac;


fract input() //funtion to take input 
{
	fract f;
	printf("Enter numerator: ");
	scanf("%d",&f.numerator);
	printf("Enter denominator: ");
	scanf("%d",&f.denominator);
	return f;
}

int gcd(int x, int y) //function to reduce the fraction
{
	int g=1;
  	for(int i=2; i <= x; i++)
	{
		if (x%i == 0 && y%i ==0)
		g = i;
	}
	return g;
}

fract calcfraction(fract x, fract y) //function to calculate fraction and reduce it
{
    	fract sum;
    	sum.numerator = ((x.numerator * y.denominator) + (y.numerator * x.denominator));
    	sum.denominator = (x.denominator * y.denominator);
    	int g = gcd(sum.numerator, sum.denominator);
    	sum.numerator = (sum.numerator/g);
    	sum.denominator = (sum.denominator/g);
    	return sum;
}

fract n_fractions(int n, nfrac store) //function calculates sum of n fractions 
{
    fract res;
    res.numerator=store.nf[0].numerator;
    res.denominator=store.nf[0].denominator;
	for(int i=1;i<n;i++)
	{
		res = calcfraction(res, store.nf[i]);
    	}
    return res;
}

void print(fract res,  nfrac store,int n) //function to print the answer
{
    printf("The sum of the fractions: %d/%d",store.nf[0].numerator,store.nf[0].denominator);
    for(int i=1;i<n;i++)
    	{
    	    printf(" + %d/%d",store.nf[i].numerator,store.nf[i].denominator);
    	}
    	printf(" =  %d / %d\n", res.numerator, res.denominator);
}


nfrac input_n(int n)
{
    nfrac store;
    for(int i=0;i<n;i++)
    {
        printf("Enter fraction %d:\n",i+1);
        store.nf[i]=input();
    }
    return store;
}

int main() 
{
    	int n;
    	printf("Enter the number of fraction:");
    	scanf("%d",&n);
    	fract res;
    	nfrac store=input_n(n);
    	res = n_fractions(n,store);
    	print(res,store,n);
    	return 0;
}
